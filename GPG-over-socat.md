# GPG over socat

## Setup

- socat and gpg installed
- two computers in the same LAN
- a file to send

## Sharing PGP public keys

TODO

## Configuring trust

TODO

## Receiver (server)

- The receiver opens a port to listent for the PGP encrypted message
- When the message is received and the socket finished, gpg will decrypt the contents

Example:

```console
socat -dd tcp-l:8080,reuseaddr system:'gpg --decrypt > secretfile'
```

The socket is closed when the sender is finished sending

## Sender (client)

- The sender encrypts the data for the receiver with their public key and pipes the ciphertext to socat
- Note that gpg will ask if you trust the recipient if you have not [configured trust](#configuring-trust)

```console
gpg --encrypt -o- -r recipient@exampel.com path/to/secretfile |socat -dd - tcp:<serverip>:<port>
```
